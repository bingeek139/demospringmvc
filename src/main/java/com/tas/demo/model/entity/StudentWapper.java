package com.tas.demo.model.entity;

import java.sql.Date;
import java.util.StringTokenizer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentWapper {
	private Integer id;
	private String fullName;
	private String  dateOfBirth;
	private String address;
	private String school;
	public Student toStudent() {
		if(id != null)
		return new Student(id,fullName, fomatDate(dateOfBirth), address, school);
		else {
			return new Student(fullName, fomatDate(dateOfBirth), address, school);
		}
	}
	private Date fomatDate(String s) {
		StringTokenizer tokenizer=new StringTokenizer(s,"-");
			 
			String day = (String) tokenizer.nextElement();
			String month = (String) tokenizer.nextElement();
			String year= (String) tokenizer.nextElement();
			Date date=new Date(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
			
			return date;
	}
	
}
