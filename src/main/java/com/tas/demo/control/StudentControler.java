package com.tas.demo.control;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tas.demo.model.dao.impl.StudentDao;
import com.tas.demo.model.entity.Student;
import com.tas.demo.model.entity.StudentWapper;

@Controller
public class StudentControler {
	@Autowired
	StudentDao st;

	@RequestMapping("/")
	public String index(ModelMap model) {
		List<Student> list=st.getAll();
		model.addAttribute("list",list);
		System.out.println("Lấy danh sách Sinh Viên");
		return "index";
	}


	
	@RequestMapping("/add")
	public String addStudent(Model model) {
		System.out.println("Thêm sinh Viên");
		model.addAttribute("command",new StudentWapper());
		return "ViewAdd";
	}
	@RequestMapping(value = {"/addStudent"} ,method = RequestMethod.POST)
	public String addStudentDatabase(@ModelAttribute("stu") StudentWapper student) {
		System.out.println("Thêm Sinh Viên thành công");
		st.add(student.toStudent());
		return "redirect: add";
	}
	@RequestMapping("/update/{id}")
	public String update(@PathVariable int id,Model model) {
		Student student=st.searchById(id);
	 	System.out.println("find"+ id);
	 	this.id=id;
	 	StudentWapper wapper=student.toWapper();
	 	
	
		model.addAttribute("command",wapper);

		return "edit";
	}
	@RequestMapping("/updates")
	public String update2(@ModelAttribute StudentWapper studentWapper) {
		System.out.println("update"+studentWapper.getId());
		Student student=studentWapper.toStudent();
		student.setId(this.id);
		st.update(student);
		System.out.println("update "+ id + "thành công");
			return "redirect: /demo ";
	}
	private int id;
	
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable int id,RedirectAttributes attributes) {
		st.deleteById(id);
		System.out.println("delete " + id);

		return "redirect: /demo ";
	}
	
	@RequestMapping("/deletemany")
	public String deleteMany(HttpServletRequest request) {
			List<Student> list=st.getAll();
			List<Integer> listId=new ArrayList<Integer>();
			for (Student student : list) {
				if(request.getParameter(String.valueOf(student.getId())) != null) {
					listId.add(student.getId());
				}
			}
			System.out.println(list.size());
			if(listId.size() >0) {
				st.deleteManyById(listId);
			}
			return "redirect: /demo ";
	}
}
