<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<h1>Student Manager</h1>

	<form action="/demo/add" method="get">
		<<input type="submit" value="Thêm sinh viên">
		</form>
	<table border="2" width="70%" cellpadding="2">
		<tr>
			<th>Tick</th>
			<th>Id  </th>
			<th>Họ và tên</th>
			<th>Ngày Sinh</th>
			<th>Trường</th>
			<th>Địa chỉ</th>
			<th>Edit</th>
			<th>Delete</th>
			
		</tr>
		<form action="deletemany" method="POST">
			<c:forEach var="stu" items="${list}">
				<tr>
					<td><input type="checkbox" name="${stu.id}"  /> </td>
					<td> ${stu.id}</td>
					<td>${stu.fullName}</td>
					<td>${stu.dateOfBirth}</td>
					<td>${stu.address}</td>
					<td>${stu.school}</td>
					<td><a href="/demo/update/${stu.id}"
						style="text-decoration: none;">Edit</a></td>
					<td><a href="/demo/delete/${stu.id}"
						style="text-decoration: none;">Delete</a></td>
					
				</tr>
			</c:forEach>
			<br>
			
			<input type="submit" value="Xóa sinh viên đã tích">
		</form>
	</table>
	<br />
</body>
</html>